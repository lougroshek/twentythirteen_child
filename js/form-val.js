(function($) {
    // var jQuery = $.noConflict(true); // Don't do this, because it breaks other plugins.
    var $ = jQuery;
    // console.log('test');
    // Because WordPress refuses to pass HTML form validation
    // fields through in the $field object, I will inject them on page load.
    // It's your fault, WordPress.
    $('document').ready(function() {
        if ($('input#billing_licence_number').length >= 1) {
            console.log('form field exists');
            $('input#billing_licence_number').attr({
                pattern: "[A-Z]{2}[0-9]{5,8}",
                minlength: '7',
                maxlength: '10'
            });
        }
    });

})(jQuery);
