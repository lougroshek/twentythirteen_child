<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
        <!--<header class="entry-header">
        	<h1 class="entry-title" style="padding-top: 15px;">Massage 360&deg;</h1>
        </header>
        <!--<div class="entry-content" style="margin-bottom: 10px;">
        Welcome to Massage 360&deg;, your resource for industry news, professional tips and ideas, and practical advice you can use in your clinic. Check out some of our most recent posts below.
        <br /><br />
        <strong>Have a tip, idea, story or news item you'd like us to share with your community? <a href="contact-us">Contact us</a> and we'll be sure to include it!</strong></div>-->
		<?php if ( have_posts() ) : ?>
			<div class="entry-content" id="indexContent">
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
                <div style=" display: inline-block; margin: 0 0 1em; width: 100%;">
                    <div style="margin: 0 auto; width: 90%;">
                    <!--<header class="entry-header">
                        <h3 class="entry-title">-->
                        <h3 style="margin-bottom: 5px;">
                            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                        </h3>
                        <div class="entry-meta" style="padding: 0">
                            <?php twentythirteen_entry_meta(); ?>
                        </div><!-- .entry-meta -->
                    <!--</header>-->
                    <!--<div class="entry-content">-->
                    <?php if (has_post_thumbnail()){ ?>
                        <div style="width: 100%; float: left; padding: 15px 0;">
                            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'full' ); ?></a>
                        </div>
                    <?php } ?>
                        <div style="width: 100%; float: left;"> 
                         <?php the_excerpt(); ?>
                        </div>
                    </div>
                </div>
            
            <?php //get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
			</div>
			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>