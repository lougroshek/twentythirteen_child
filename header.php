<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta property="og:image" content="<?php echo get_stylesheet_directory_uri();?>/images/april1-hand-tree-resized-150.png" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
    
    <?php wp_enqueue_script("jquery"); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1540860466160922&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
        	<div id="miniMenu" style="padding-right: 15px;">
            <?php if (is_user_logged_in() ) { echo '<div class="noShow">'; } ?><span style="color: #597a0f">Like us on FB</span> <div style="overflow: hidden; padding-top: 3px" class="fb-like" data-href="https://massagecelearningtree.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div> &nbsp; | &nbsp; <?php if (is_user_logged_in() ) { echo '</div>'; } ?>
            <?php if ( is_user_logged_in() ) { ?>
                <a style="z-index: 1000000" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><img src="https://massagecelearningtree.com/wp-content/uploads/2015/02/my-account.png"><?php _e('My Account','woothemes'); ?></a> &nbsp; | &nbsp; <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>/customer-logout/" title="<?php _e('Logout','woothemes'); ?>"><img src="https://massagecelearningtree.com/wp-content/uploads/2015/02/logout.png"/><?php _e('Logout','woothemes'); ?></a>
             <?php } 
             else { ?>
                <a style="z-index: 1000000" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login','woothemes'); ?>"><img src="https://massagecelearningtree.com/wp-content/uploads/2015/02/login.png" /><?php _e('Login','woothemes'); ?></a>
             <?php } ?>
             <?php global $woocommerce; ?>
 			 <?php if($woocommerce->cart->cart_contents_count > 0){ ?>
			&nbsp; | &nbsp; <a style="z-index: 1000000" class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo '<img src="https://massagecelearningtree.com/wp-content/uploads/2015/02/shopping-cart.png" />'; echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
             <?php } ?>
            </div>
			<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                <div id="headerBanner">
                	<div id="headerImage">
                    	
                    <img src='<?php echo get_stylesheet_directory_uri();?>/images/april1-hand-tree-resized-150.png' alt="Massage CE Learning Tree" />
                    	
                    </div>
                    <div id="headerText">
                        <h1 class="site-title"><?php echo bloginfo( 'name' ); ?></h1>
                        <h2 class="site-description"><?php echo bloginfo( 'description' ); ?></h2>
                    </div>
                    <div id="NCBTMB">
                    	<img src="<?php echo get_stylesheet_directory_uri();?>/images/ncbtmb_logo_150x150.png" height="140px" style="padding: 0px 25px 0 0" alt="NCBTMB Approved Provider" />
                    </div>
                </div>
			</a>

			<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<button class="menu-toggle"><?php _e( 'Menu', 'twentythirteen' ); ?></button>
					<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
					<!--<div class="login-form">
						<?php if ( is_user_logged_in() ) { ?>
                            <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a> &nbsp; | &nbsp; <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>/customer-logout/" title="<?php _e('Logout','woothemes'); ?>"><?php _e('Logout','woothemes'); ?></a>
                         <?php } else { ?>
                            <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login','woothemes'); ?>"><?php _e('Login','woothemes'); ?></a>
                         <?php } ?>
                    </div>-->
                    <?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'nav-menu' ) ); ?>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->
		</header><!-- #masthead -->

		<div id="main" class="site-main">
