<?php
/*
Template Name: Alabama Courses
*/

get_header(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php /* The loop */ ?>
            <?php 
			//NICK: THIS IS THE LOOP FOR THE REGULAR POST CONTENT
			while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post -->
			<?php endwhile; ?>
			
			<?php
			//THIS IS WHERE THE COURSE LIST STARTS
				$args = array(
				  'post_type' => 'course',
				  'tax_query' => array(
					array(
					  'taxonomy' => 'course_category',
					  'field' => 'slug',
					  'terms' => 'alabama' //<--- ENTER THE SLUG FOR THE STATE HERE
					)
				  )
				);
				$products = new WP_Query( $args );
				if( $products->have_posts() ) {
				  while( $products->have_posts() ) {
					$products->the_post();
					
					// get the post ID 
					  $price = get_post_meta( get_the_ID(), 'bwq_course_price', true );
					?>
					  <div class='entry-content'>
                      	<div class='courseListingWrap'>
                            <div class='courseListing'>
                                <div class='courseImage'>
                                <?php if ( has_post_thumbnail() ) { // check if the post has a featured image
                                        the_post_thumbnail(array(250, 250));
                                    } else {
										echo '<img src="'. get_stylesheet_directory_uri() . '/images/cap-resized.jpg" />';	
									}
                                ?>
                                </div>
                                <div class='courseDescription'>
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>
                                    <?php the_excerpt() ?>
                                    <?php if(isset($price) && $price != ""){ //echo the price if one was saved
                                    	echo '<p>$'. $price .'</p>';
									}?>
                                </div>
                            </div>
                         </div>
					  </div>
					<?php
				  }
				}
				else {?>
					  <div class='entry-content'>
						<p>No courses were found for this state.</p>
					  </div>
				<?php }
			  ?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>