<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php get_sidebar( 'main' ); ?>

			<div class="site-info">
				<?php do_action( 'twentythirteen_credits' ); ?>
				&copy; <?php echo date('Y');?> Massage CE Learning Tree &nbsp; | <a href="/privacy-policy"/>Privacy Policy</a> &nbsp; | &nbsp; <a href="/contact-us/">Contact Us</a> &nbsp; | &nbsp; <a href="https://massagecelearningtree.com/mission-statement/">Mission Statement</a> &nbsp; | &nbsp; Site Design: <a href="http://bwqenterprises.ca" target="_blank">BWQ Enterprises</a>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>