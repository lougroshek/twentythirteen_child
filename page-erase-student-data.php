<?php
/*
Template Name: Erase Student Data
*/

get_header(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
                        <?php global $wpdb;
						// this adds the prefix which is set by the user upon instillation of wordpress
						$table_name = $wpdb->prefix . "usermeta";
						// this will get the data from your table
						$dateStringtoCheck = strtotime("-1 year");
						echo $dateStringtoCheck."<br />";
						echo date("Y-m-d", $dateStringtoCheck);
						
						$retrieve_data = $wpdb->get_results( 'SELECT * FROM ce_usermeta WHERE meta_key = "course_1103_access_from" OR meta_key = "course_796_access_from" OR meta_key = "course_636_access_from" OR meta_key = "course_509_access_from" OR meta_key = "course_454_access_from" OR meta_key = "course_3782_access_from" OR meta_key = "course_209_access_from" OR meta_key = "course_352_access_from" OR meta_key = "course_371_access_from" OR meta_key = "course_388_access_from" OR meta_key = "course_2622_access_from" OR meta_key = "course_2757_access_from") AND meta_value < $dateStringtoCheck', OBJECT );
						echo "<ul>";
						foreach ($retrieve_data as $retrieved_data){ ?>
						<li><?php echo $retrieved_data->user_id; ?></li>
						<!--<li><?php //echo $retrieved_data->another_column_name;?></li>
						<li><?php //echo $retrieved_data->as_many_columns_as_you_have;?></li>-->
						<?php 
						}
						?>
						</ul>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				<?php comments_template(); ?>
			<?php endwhile; ?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>