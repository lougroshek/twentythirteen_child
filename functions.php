<?php

register_nav_menu( 'secondary', __( 'Second Level Menu', 'twentythirteen' ) );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'form-val', get_stylesheet_directory_uri() . '/css/form-val.css' );
    // For form validation for the billing license field.
    wp_enqueue_script(
        'form-val',
        get_stylesheet_directory_uri() . '/js/form-val.js',
        array('jquery')
    );
}

function ce_load_custom_wp_admin_style() {
    // For form validation for the billing license field.
    wp_enqueue_style( 'form-val', get_stylesheet_directory_uri() . '/css/form-val.css' );
    wp_enqueue_script(
        'form-val',
        get_stylesheet_directory_uri() . '/js/form-val.js',
        array('jquery')
    );
}
add_action( 'admin_enqueue_scripts', 'ce_load_custom_wp_admin_style' );

remove_action('wp_footer', 'wp_func_jquery');

/**
 * Change on single product panel "Product Description"
 * since it already says "features" on tab.
 */
add_filter('woocommerce_product_description_heading',
'isa_product_description_heading');

function isa_product_description_heading() {
    return __('Course Description', 'woocommerce');
}

/** Add custom tabs to courses **/
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {

	// Adds the new tab

	$tabs['course_materials'] = array(
		'title' 	=> __( 'Materials', 'woocommerce' ),
		'priority' 	=> 50,
		'callback' 	=> 'woo_new_product_tab_content_materials'
	);

	$tabs['course_approvals'] = array(
		'title' 	=> __( 'Approvals', 'woocommerce' ),
		'priority' 	=> 55,
		'callback' 	=> 'woo_new_product_tab_content_approvals'
	);

	return $tabs;

}
function woo_new_product_tab_content_materials() {

	// The new tab content

	echo '<h2>Materials</h2>';
	echo '<p>All courses are completely online. Each course exam is graded instantly and, once you pass the multiple choice exam with 70%, your "Certificate of Completion" is immediately available for printing. All course information is available for one year from the date of purchase. The student has an unlimited number of opportunities to pass the exam.</p>
	<p>Exams consist of either 5, 10 or 15 multiple choice questions. The exam may be downloaded and printed in advance for easier studying.</p>';

}
function woo_new_product_tab_content_approvals() {

	// The new tab content

	echo '<h2>Approvals</h2>';
	echo '<p>All of our courses have NCBTMB approval.</p>';

}

// Adds custom billing number field to woocommerce billing info.
// Hook in
// add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
// // Our hooked in function - $fields is passed via the filter!
// function custom_override_checkout_fields( $fields ) {
//      $fields['billing']['licence_number'] = array(
//         'label'     => __('License Number (for use on certificates)', 'woocommerce'),
//     'placeholder'   => _x('Recommended format: MA12345', 'placeholder', 'woocommerce'),
//     'required'  => false,
//     'class'     => array('form-row-wide'),
//     'clear'     => false
//      );
//      return $fields;
// }

/**
 * Update the user meta with field value
 **/
// add_action('woocommerce_checkout_update_order_meta', 'custom_checkout_field_update_order_meta');
//
// function custom_checkout_field_update_order_meta( $order_id ) {
// 	// $current_user = wp_get_current_user();
//     // if ($_POST['billing_licence_number']) update_user_meta( $current_user->ID, 'billing_licence_number', esc_attr($_POST['billing_licence_number']));
//     $billing_license_number = ! empty( $_POST['billing_licence_number'] ) ? wc_clean( $_POST['billing_licence_number'] ) : '';
//     // echo '<h3>billing license number = '.$billing_license_number;
//     $user_id = get_current_user_id();
//     // echo '<h3>$user_id = '.$billing_license_number;
//     // Save to user meta in db
//     update_user_meta( $user_id, 'billing_licence_number', $billing_license_number );
// }


// add_action('woocommerce_checkout_update_user_meta', 'my_custom_checkout_field_update_user_meta');
//
add_action('woocommerce_checkout_update_order_meta', 'ce_woocommerce_checkout_update_user_meta');

function ce_woocommerce_checkout_update_user_meta() {
    // error_log('ce_woocommerce_save_account_details()\n');
    $billing_license_number = ! empty( $_POST['billing_licence_number'] ) ? wc_clean( $_POST['billing_licence_number'] ) : '';
    $user_id = get_current_user_id();
    // Save to user meta in db
    update_user_meta( $user_id, 'billing_licence_number', $billing_license_number );
}
// add_action( 'woocommerce_save_account_details', 'ce_woocommerce_save_account_details' );


add_filter('woocommerce_billing_fields', 'custom_billing_fields', 20 );
function custom_billing_fields( $fields ) {
    // Add license field.
    $fields['billing_licence_number'] = array(
        'label'     => __('License Number (for use on certificates)', 'woocommerce'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => true,
        'pattern'   => '[A-Z]{2}[0-9]{5,8}',
        'minlength' => '7',
        'maxlength' => '10',
        'placeholder' => 'Recommended format: MA12345'
    );
    return $fields;
}

add_filter('woocommerce_address_to_edit', 'reorder_woocommerce_address_fields', 10, 2);
function reorder_woocommerce_address_fields( $address, $load_address) {
    $new_address['billing_first_name'] = $address['billing_first_name'];
    $new_address['billing_last_name'] = $address['billing_last_name'];
    $new_address['billing_company'] = $address['billing_company'];
    $new_address['billing_email'] = $address['billing_email'];
    $new_address['billing_phone'] = $address['billing_phone'];
    $new_address['billing_country'] = $address['billing_country'];
    $new_address['billing_address_1'] = $address['billing_address_1'];
    $new_address['billing_postcode'] = $address['billing_postcode'];
    $new_address['billing_city'] = $address['billing_city'];
    $new_address['billing_state'] = $address['billing_state'];

    // Add license field.
    $user_id = get_current_user_id();
    $user_meta_billing_licence_number = get_user_meta( $user_id, 'billing_licence_number', true );
    $new_address['billing_licence_number'] = array(
        'label'     => __('License Number (for use on certificates)', 'woocommerce'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => true,
        'pattern'   => '[A-Z]{2}[0-9]{5,8}',
        'minlength' => '7',
        'maxlength' => '10',
        'placeholder' => 'Recommended format: MA12345',
        'value'     => $user_meta_billing_licence_number
    );

    return $new_address;
}

// define the woocommerce_customer_save_address callback
function ce_woocommerce_customer_save_license( $user_id, $load_address ) {
    // Fetch billing number from post data
    $billing_license_number = ! empty( $_POST['billing_licence_number'] ) ? wc_clean( $_POST['billing_licence_number'] ) : '';
    // Save to user meta in db
    update_user_meta( $user_id, 'billing_licence_number', $billing_license_number );
};

// add the action
add_action( 'woocommerce_customer_save_address', 'ce_woocommerce_customer_save_license', 10, 2 );
add_action( 'woocommerce_save_account_details', 'ce_woocommerce_customer_save_license', 10, 2 );
// add_action('woocommerce_checkout_update_order_meta', 'ce_woocommerce_customer_save_license');
// add_action('woocommerce_after_checkout_validation', 'ce_woocommerce_customer_save_license', 10, 2 );


function fb_add_custom_user_profile_fields( $user ) {
?>
	<h3><?php _e('Extra Profile Information', 'twentythirteen_child'); ?></h3>

	<table class="form-table">
		<tr>
			<th>
				<label for="billing_licence_number"><?php _e('License Number', 'twentythirteen_child'); ?>
			</label></th>
			<td>
				<input type="text" placeholder="Recommended format: MA12345" name="billing_licence_number" id="billing_licence_number" pattern="[A-Z]{2}[0-9]{5,8}" maxlength="10" minlength="7" value="<?php echo get_user_meta( $user->ID, 'billing_licence_number', true); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Update user billing address.', 'twentythirteen_child'); ?></span>
			</td>
		</tr>
	</table>
<?php }

function fb_save_custom_user_profile_fields( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) ) {
		return FALSE;
    }
    $billing_license_number = ! empty( $_POST['billing_licence_number'] ) ? wc_clean( $_POST['billing_licence_number'] ) : '';
	update_user_meta( $user_id, 'billing_licence_number', $billing_license_number );
}

add_action( 'show_user_profile', 'fb_add_custom_user_profile_fields' );
add_action( 'edit_user_profile', 'fb_add_custom_user_profile_fields' );

add_action( 'personal_options_update', 'fb_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'fb_save_custom_user_profile_fields' );


// tie into the WooCommerce MailChimp 'ss_wc_mailchimp_subscribe_merge_vars' action filter to add the MailChimp merge vars for WooCommerce Order fields
function ss_wc_send_woocommerce_fields( $merge_vars, $order_id ) {
// retrieve the WooCommerce Order
$order = new WC_Order($order_id);

// Add merge variables to MailChimp call

$merge_vars['STATE'] = $order->billing_state;

return $merge_vars;
}
add_filter( 'ss_wc_mailchimp_subscribe_merge_vars' , 'ss_wc_send_woocommerce_fields', 10 , 2 );

/**
* Auto Complete all WooCommerce orders.
* Add to theme functions.php file
*/

add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
function custom_woocommerce_auto_complete_order( $order_id ) {
    global $woocommerce;

    if ( !$order_id )
        return;
    $order = new WC_Order( $order_id );
    $order->update_status( 'completed' );
}

function wooc_save_extra_register_fields( $customer_id ) {
	if ( isset( $_POST['first_name'] ) ) {
		// WordPress default first name field.
		update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['first_name'] ) );

	}

	if ( isset( $_POST['last_name'] ) ) {
		// WordPress default last name field.
		update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['last_name'] ) );
	}
}

add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );

/** SHOW ALL PRODUCTS ON ONE PAGE **/
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 100;' ), 20 );

/****** TO SUBMIT NOTICE TO CE BROKER **********/

add_action('learndash_quiz_completed', function($data) {

	global $wpdb;
    // Placement.
    error_log('learndash_quiz_completed');
    // Print out passed data on course and quiz.
    // error_log(var_export($data, true));

	// Must be logged in
	if ( ! is_user_logged_in() ) return;

        error_log('user is logged in');

    	$user_id = get_current_user_id();
        error_log('$user_id is '.$user_id);
    	$user_info = get_userdata($user_id);
    	$userEmail = $user_info->user_email;
        $admin_email = get_option('admin_email');

		$courseID = $data['course']->ID;
        error_log('$courseID is '.$courseID);
		$courseName = $data['course']->post_title;
        error_log('$courseName is '.$courseName);
		$quizID = $data['quiz']->ID;
        $quizName = $data['quiz']->post_title;
        error_log('$quizID is '.$quizID);
		$score = $data['percentage'];
		$currDate = date('Y-m-d', strtotime("-5 hours"));
		$CEBrokerDate = date('m/d/Y', strtotime("-5 hours"));

        $first = get_user_meta( $user_id, "first_name", true);
        $last = get_user_meta( $user_id, "last_name", true);
        $user_state = get_user_meta($user_id, 'billing_state', true);
        $license = get_user_meta( $user_id, "billing_licence_number", true);
        // Do some checking here so recipient can tell if
        // a useful value has not been provided.
        $reported_state = !empty($user_state) ? $user_state : 'User state not provided.';
        $reported_license = !empty($license) ? $license : 'User license not provided.';


        $not_evaluations = array(324, 362, 383, 392, 493, 590, 706, 825, 2243, 2863, 2925, 3996, 6140);

        if (in_array($quizID, $not_evaluations, false)) {
            error_log('The completed quiz is not an evaluation.');

            // Sending initial completion email.
            error_log('Sending initial completion email.');
            //Email Jeannette with student information
            $to = $admin_email;
            $subject = "CE Broker Integration: Quiz Completion Event";
            $message = "The following student has completed a quiz:\n";
            $message .= " \n Student name: ".$first." ".$last;
            $message .= " \n Student email: ".$userEmail;
            $message .= " \n Quiz: ".$quizName;
            $message .= " \n Score: ".$score.'%';
            $message .= " \n Date completed: ".$currDate;
            $message .= " \n Course: ".$courseName;
            $message .= " \n Student state: ".$reported_state;
            $message .= " \n Student license Number: ".$reported_license;
            wp_mail( $to, $subject, $message );

    		if($score < 70) {
    			//quiz not passed
    			error_log('quiz not passed');
    		} else {
    			//quiz passed
    			error_log('quiz passed');

    			//check if submission has happened in the past already
    			$result = $wpdb->query( $wpdb->prepare( "SELECT * FROM ce_users_cebroker_submissions WHERE user_ID = %d AND course_ID = %d", $user_id, $courseID) );
    			$numRows = $wpdb->num_rows;

    			if($numRows > 0){
    				//record already exists
    				error_log('record already exists');
    			} else {
    				//if not, submit to CEBROKER
                    error_log('no record, submitting to CE Broker');
    				//check if student is licensed in Florida
    				// $stateKey = "billing_state";
    				// $state = $wpdb->get_results( $wpdb->prepare( "SELECT meta_value FROM ce_usermeta WHERE meta_key = %s AND user_id = %d", $stateKey, $user_id), OBJECT );
    				// $userState = $state[0]->meta_value;
    				$user_id = get_current_user_id();
    				$user_state = get_user_meta($user_id, 'billing_state', true);
                    error_log('user ID = '.$user_id);
                    error_log('$user_state = '.$user_state);
    				// if($userState == "Florida"){
                    if($user_state == "FL"){
                        error_log('User state is Florida');
    					$first = strtoupper(get_user_meta( $user_id, "billing_first_name", true));
    					$last = strtoupper(get_user_meta( $user_id, "billing_last_name", true));
    					$license = get_user_meta( $user_id, "billing_licence_number", true);

    					//if so, validate licence number
    					$licenseFormatValid = false;

    					//remove spaces, #s, -s, and capitalize all letters
    					$licenseReformat = str_replace(str_split(' #-'), "", $license);
    					$licenseReformat = strtoupper($licenseReformat);

    					//regex expressions for validation
    					$correct = '/^MA[0-9]{5,8}$/';
    					$missingMA = '/^[0-9]{5,8}$/';

    					if(preg_match($correct, $licenseReformat)) {
    						//format is valid
    						error_log('format is valid');
    						$licenseFormatValid = true;
    					} elseif(preg_match($missingMA, $licenseReformat)) {
    						//format missing MA
    						error_log('format missing MA');
    						$licenseReformat = "MA".$licenseReformat;
    						$licenseFormatValid = true;
    					} else {
    						//format is incorrect
                            error_log('format is incorrect');
    						//Email Jeannette with student information
    						$to = $admin_email;
    						$subject = "CE Broker Integration: Student License Number Format Unknown";
    						$message = "The following student's license number doesn't seem to fit the format for Florida: \n";
    						$message .= " First name: ".$first." \n Last name: ".$last." \n Email: ".$userEmail." \n License Number: ".$licenseReformat." \n Course Completed: ".$courseName." \n Date Completed: ".$currDate;
    						wp_mail( $to, $subject, $message );

                            // Email student
                            // $to = $userEmail;
                            // $subject = "CE Broker Integration: Error with license number";
                            // $message = "An error occurred when the site ". get_option('siteurl') ." tried to forward your certification data to the CE Broker platform. It appears that your license number is not in a format that is valid for the state of Florida. Please check your profile settings for state and license number, and contact the site administrator at ". $admin_email .".\n\n";
                            // $message .= " Your name: ".$first." ".$last." \n";
                            // $message .= " Your state: ".$reported_state." \n";
                            // $message .= " Your license: ".$reported_license." \n";
                            // $message .= " Course Completed: ".$courseName." \n";
                            // $message .= " Date Completed: ".$currDate." \n";
                            // wp_mail( $to, $subject, $message );
    					}

    					if($licenseFormatValid == true){

    						$CEBrokerStudentValid = false;
                            error_log('validating with CE Broker');
    						//validate via CEBroker API if Florida, if valid, set CEBrokerStudentValid = true

    						$xml = array("InXML" => "<?xml version='1.0'?><licensees id_parent_provider='14566' upload_key='JCU33040'>
    											<licensee>
    												<licensee_profession>MA</licensee_profession>
    												<licensee_number>".$licenseReformat."</licensee_number>
    											</licensee>
    									   </licensees>");
    						$xml = http_build_query($xml);

    						$url = "http://webservices.cebroker.com/CEBrokerWebService.asmx/UploadXMLString";
    						$headers = array(
    							"POST /CEBrokerWebService.asmx/UploadXMLString HTTP/1.1"
    							);

    					 	$ch = curl_init();
    					  	//curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0);
    						curl_setopt( $ch, CURLOPT_URL, $url );
    					  	curl_setopt( $ch, CURLOPT_POST, true );
    					  	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    					  	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    					  	curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
    						curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true);
    					  	$curlResult = curl_exec($ch);
    					  	curl_close($ch);

    						if (strpos($curlResult, 'licensee valid="true"') !== false) {
                                error_log('CE Broker student license is valid');
    							$CEBrokerStudentValid = true;
    						} else {
    							$CeBrokerStudentValid = false;
                                error_log('CE Broker student license is __NOT__ valid. Sending email message.');
    							//Email Jeannette with student information
    							$to = $admin_email;
    							$subject = "CE Broker Integration: Invalid Student Florida License Number";
    							$message = "CEBroker has reported the following student's license number is not valid in Florida: \n";
    							$message .= " First name: ".$first." \n Last name: ".$last." \n Email: ".$userEmail." \n License Number: ".$licenseReformat." \n Course Completed: ".$courseName." \n Date Completed: ".$currDate;
    							wp_mail( $to, $subject, $message );
                                // Log to debug logs.
                                error_log('License number for '.$first.' '.$last.' With license code '.$licenseReformat.' was rejected as not valid for course '.$courseName.', quiz number '.$quizID.'.');

                                // Email student
                                // $to = $userEmail;
                                // $subject = "CE Broker Integration: Error with license number";
                                // $message = "An error occurred when the site ". get_option('siteurl') ." tried to forward your certification data to the CE Broker platform. It appears that although you have selected Florida as your state of certification, your license number is not valid for the state of Florida. Please check your profile settings for state and license number, and contact the site administrator at ". $admin_email .".\n\n";
                                // $message .= " Your name: ".$first." ".$last." \n";
                                // $message .= " Your state: ".$reported_state." \n";
                                // $message .= " Your license: ".$reported_license." \n";
                                // $message .= " Course Completed: ".$courseName." \n";
                                // $message .= " Date Completed: ".$currDate." \n";
                                // wp_mail( $to, $subject, $message );
    						}

                            error_log('converting to CE Broker course ID. Quiz ID = '.$quizID);
                            $CEBrokerCourseID = 0;
    						if($CEBrokerStudentValid){
                                error_log('$CEBrokerStudentValid is valid, converting to CE Broker course ID.');
    							//convert course code to CEBroker Course Code
    							switch($quizID){
    								//immune
    								case 324:
    									$CEBrokerCourseID = "504414";
    									break;
    								//HIV
    								case 362:
    									$CEBrokerCourseID = "494043";
    									break;
    								//Chronic
    								case 383:
    									$CEBrokerCourseID = "492239";
    									break;
    								//Diabetes
    								case 392:
    									$CEBrokerCourseID = "492237";
    									break;
    								//Aromatherapy
    								case 493:
    									$CEBrokerCourseID = "504504";
    									break;
    								//Insight
    								case 590:
    									$CEBrokerCourseID = "490919";
    									break;
    								//Prevention
    								case 706:
    									$CEBrokerCourseID = "509346";
    									break;
    								//Florida
    								case 825:
    									$CEBrokerCourseID = "488659";
    									break;
    								//Ethics
    								case 2243:
    									$CEBrokerCourseID = "492223";
    									break;
    								//Cultural
    								case 2863:
    									$CEBrokerCourseID = "537220";
    									break;
    								//Business
    								case 2925:
    									$CEBrokerCourseID = 0;
    									break;
    								//test course
    								case 3996:
    									$CEBrokerCourseID = 635771;
    									break;
                                    // AG test course
    								case 6140:
    									$CEBrokerCourseID = 635771;
    									break;
    								//if not an exam
    								default:
    									$CEBrokerCourseID = 0;
    									break;
    							}

                                error_log('$CEBrokerCourseID = '.$CEBrokerCourseID);

    							//if valid license, and exam valid, submit course completion
    							if($CEBrokerCourseID !== 0){
                                    error_log('Course has CE Broker analog, sending data.');
    								$xml = array("InXML" => "<?xml version='1.0'?><rosters id_parent_provider='14566' upload_key='JCU33040'>
    									<roster>
    										<id_provider>14566</id_provider>
    										<id_course>".$CEBrokerCourseID."</id_course>
    										<attendees>
    											<attendee>
    												<licensee_profession>MA</licensee_profession>
    												<licensee_number>".$licenseReformat."</licensee_number>
    												<cebroker_state>FL</cebroker_state>
    												<first_name>".$first."</first_name>
    												<last_name>".$last."</last_name>
    												<date_completed>".$CEBrokerDate."</date_completed>
    											</attendee>
    										</attendees>
    									</roster>
    								</rosters>");

                                    error_log($xml['InXML']);

    								$xml = http_build_query($xml);

    								$url = "http://webservices.cebroker.com/CEBrokerWebService.asmx/UploadXMLString";
    								$headers = array(
    									"POST /CEBrokerWebService.asmx/UploadXMLString HTTP/1.1"
    									);

    								$ch = curl_init();
    								//curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0);
    								curl_setopt( $ch, CURLOPT_URL, $url );
    								curl_setopt( $ch, CURLOPT_POST, true );
    								curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    								curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    								curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
    								curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true);
    								$curlResult = curl_exec($ch);
    								curl_close($ch);

    								$errorCodeEmpty = '/ErrorCode=""/';
    								$errorCodeNumber = '/ErrorCode="([^"]+)"/';

    								$status = "";

    								if(preg_match($errorCodeEmpty, $curlResult)){
    									$status = "success";
                                        error_log('CE Broker submission success!');

                                        //Email Jeannette with student information
    									$to = $admin_email;
    									$subject = "CE Broker Integration: Submission Success!";
    									$message = "CEBroker has reported a successful submission for: \n";
    									$message .= " First name: ".$first." \n Last name: ".$last." \n Email: ".$userEmail." \n License Number: ".$licenseReformat." \n Course Completed: ".$courseName." \n Date Completed: ".$currDate. "\n";
                                        error_log($message);
    									wp_mail( $to, $subject, $message );

                                        // Email student
            							// $to = $userEmail;
            							// $subject = "CE Broker Integration: Submission Success!";
            							// $message = "Your certification data from ". get_option('siteurl') ." was successfully forwarded to the CE Broker platform.\n\n";
            							// $message .= " Your name: ".$first." ".$last." \n";
                                        // $message .= " Your state: ".$reported_state." \n";
                                        // $message .= " Your license: ".$reported_license." \n";
                                        // $message .= " Course Completed: ".$courseName." \n";
                                        // $message .= " Date Completed: ".$currDate." \n";
            							// wp_mail( $to, $subject, $message );

    								} else {
    									preg_match_all($errorCodeNumber, $curlResult, $errors);
    									$status = $errors[1][0];

                                        error_log('CE Broker submission ERROR. Error number = '.$status.' User '.$first.' '.$last.'. Course '.$courseName.' on '.$currDate.'.');

    									//Email Jeannette with student information
    									$to = $admin_email;
    									$subject = "CE Broker Integration: Error with submission";
    									$message = "CEBroker has reported an error with the following submission: \n";
    									$message .= " First name: ".$first." \n Last name: ".$last." \n Email: ".$userEmail." \n License Number: ".$licenseReformat." \n Course Completed: ".$courseName." \n Date Completed: ".$currDate. "\n Error Code: ".$status. "\n";
    									$message .= "Please follow up with CE Broker support directly for assistance.";
    									wp_mail( $to, $subject, $message );

                                        // Email student
            							// $to = $userEmail;
            							// $subject = "CE Broker Integration: Error with submission";
            							// $message = "An error occurred when the site ". get_option('siteurl') ." tried to forward your certification data to the CE Broker platform. Please check your profile settings for state and license number, and contact the site administrator at ". $admin_email .".\n\n";
            							// $message .= " Your name: ".$first." ".$last." \n";
                                        // $message .= " Your state: ".$reported_state." \n";
                                        // $message .= " Your license: ".$reported_license." \n";
                                        // $message .= " Course Completed: ".$courseName." \n";
                                        // $message .= " Date Completed: ".$currDate." \n";
                                        // $message .= " Error: ".$status." \n";
            							// wp_mail( $to, $subject, $message );
    								}

    								//handle response by inserting entry into ce_users_cebroker_submissions table
    								$wpdb->query( $wpdb->prepare( "INSERT INTO ce_users_cebroker_submissions ( user_ID, course_ID, date, status ) VALUES ( %d, %d, %s, %s )", $user_id, $courseID, $currDate, $status) );

    							}
    						}
    					}
    				} else {
                        error_log('User '.$first.' '.$last.' is not from Florida.');
    					//if student isn't from Florida
    					$first = strtoupper(get_user_meta( $user_id, "billing_first_name", true));
    					$last = strtoupper(get_user_meta( $user_id, "billing_last_name", true));
    					$license = get_user_meta( $user_id, "billing_licence_number", true);

    					//if so, validate licence number
    					$licenseFormatValid = false;

    					//remove spaces, #s, -s, and capitalize all letters
    					$licenseReformat = str_replace(str_split(' #-'), "", $license);
    					$licenseReformat = strtoupper($licenseReformat);

    					// $correct = '/^MA[0-9]{5}$/';
    					$correct = '/^MA[0-9]{5,8}$/';

    					if(preg_match($correct, $licenseReformat)) {
    						//format is valid
    						$licenseFormatValid = true;
    					}

    					if($licenseFormatValid){
                            error_log('User licensed in state other than FL, emailing admin.');
    						// Email Jeannette with student information
							$to = $admin_email;
							$subject = "CE Broker Integration: Non-Florida Student Course Completion";
							$message = "The following student is not licensed in Florida, and has completed the following course: \n\n";
							$message .= " First name: ".$first." \n Last name: ".$last." \n State: ".$userState." \n Email: ".$userEmail." \n License Number: ".$license." \n Course Completed: ".$courseName." \n Date Completed: ".$currDate;
							wp_mail( $to, $subject, $message );

                            // Email student
							// $to = $userEmail;
							// $subject = "CE Broker Integration: Non-Florida Student Course Completion";
							// $message = "You have completed a course on ". get_option('siteurl') .". This site automatically reports certifications for learners with Florida licensure to the CE Broker platform. Your license number is in a valid Florida format, but your state billing address is not set to Florida. As a result, we're not quite sure whether you're trying to get a Florida certification reported or not. If you are trying to earn certification in the state of Florida, please check your profile settings for state and license number, and contact the site administrator at ". $admin_email .".\n\n";
							// $message .= " Your name: ".$first." ".$last." \n";
                            // $message .= " Your state: ".$reported_state." \n";
                            // $message .= " Your license: ".$reported_license." \n";
                            // $message .= " Course Completed: ".$courseName." \n";
                            // $message .= " Date Completed: ".$currDate;
							// wp_mail( $to, $subject, $message );
    					}

    				}
    			}

    		}

        }

}, 5, 1);
