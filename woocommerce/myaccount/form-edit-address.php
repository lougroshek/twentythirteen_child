<?php
/**
 * Edit address form
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $current_user;

$page_title = ( $load_address === 'billing' ) ? __( 'Billing Address', 'woocommerce' ) : __( 'Shipping Address', 'woocommerce' );

get_currentuserinfo();

?>

<?php wc_print_notices(); ?>

<?php if ( ! $load_address ) : ?>

	<?php wc_get_template( 'myaccount/my-address.php' ); ?>

<?php else : ?>

	<form method="post">

		<h3><?php echo apply_filters( 'woocommerce_my_account_edit_address_title', $page_title ); ?></h3>

		<?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>

		<?php foreach ( $address as $key => $field ) : ?>

			<?php woocommerce_form_field( $key, $field, ! empty( $_POST[ $key ] ) ? wc_clean( $_POST[ $key ] ) : $field['value'] ); ?>

		<?php endforeach; ?>

        <?php
        // Original code by Bryan. I'm overriding on 2018-10-25 because this
        // form field is no longer saving this data.
		// added code to show checkout custom field License Number
        // $field = array(
		// 	'label'     => __('License Number (for use on certificates)', 'woocommerce'),
		// 		'placeholder'   => _x('License Number', 'placeholder', 'woocommerce'),
		// 		'required'  => false,
		// 		'class'     => array('form-row-wide'),
		// 		'clear'     => true,
        //         'placeholder' => 'MA12345'
		// 		 );
		// 		 //$value = $current_user->nip;
		// 		 $value = get_user_meta( $current_user->ID, 'billing_licence_number', true );
		// 		 woocommerce_form_field( 'billing_licence_number', $field, $value );
		?>
		<?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>

		<p>
			<input type="submit" class="button" name="save_address" value="<?php _e( 'Save Address', 'woocommerce' ); ?>" />
			<?php wp_nonce_field( 'woocommerce-edit_address' ); ?>
			<input type="hidden" name="action" value="edit_address" />
		</p>

	</form>

<?php endif; ?>
